const navigation = () => {
  return [
    // {
    //   title: 'Home',
    //   path: '/home',
    //   icon: 'mdi:home-outline'
    // },
    {
      title: 'New submissions',
      path: '/new-submissions',
      icon: 'mdi:home-outline'
    },
    {
      title: 'Published Submissions',
      path: '/published-submissions',
      icon: 'mdi:home-outline'
    },
    {
      title: 'Create new user',
      path: '/create-user',
      icon: 'mdi:home-outline'
    }

    // {
    //   title: 'Create a new release',
    //   path: '/create-release',
    //   icon: 'mdi:email-outline'
    // },
    // {
    //   title: 'Release Drafts',
    //   path: '/release-drafts',
    //   icon: 'mdi:email-outline'
    // }

    // {
    //   path: '/acl',
    //   action: 'read',
    //   subject: 'acl-page',
    //   title: 'Access Control',
    //   icon: 'mdi:shield-outline'
    // },
    // {
    //   title: 'Form',
    //   path: '/form',
    //   icon: 'mdi:transit-connection-horizontal'
    // }

    // {
    //   title: 'Create a new release',
    //   path: '/create-release',
    //   icon: 'mdi:transit-connection-horizontal'
    // }
  ]
}

export default navigation
