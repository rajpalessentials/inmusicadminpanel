export default {
  meEndpoint: '/auth/me',

  // loginEndpoint: '/jwt/login',

  loginEndpoint: 'https://api.inmusicdigital.com/v4/admin/login',

  // registerEndpoint: '/jwt/register',
  storageTokenKeyName: 'accessToken',
  onTokenExpiration: 'refreshToken' // logout | refreshToken
}
