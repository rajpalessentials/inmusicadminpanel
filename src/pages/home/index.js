// ** MUI Imports
import Card from '@mui/material/Card'
import Grid from '@mui/material/Grid'
import Typography from '@mui/material/Typography'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import Button from '@mui/material/Button'
import { useRouter } from 'next/navigation'

const NewSubmissions = () => {
  const router = useRouter()

  const publishRelease = async e => {
    const response = await axios.post()
  }

  return (
    <Grid container spacing={6}>
      <Grid item xs={4}>
        <Card>
          <CardHeader title='home 🚀'></CardHeader>
          <CardContent>
            {/* <Grid item xs={6}>
              <Button onClick={() => router.replace('/home/view')}>View</Button>
            </Grid>
            <Grid item xs={6}>
              <Button onClick={publishRelease}>Publish</Button>
            </Grid> */}
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  )
}

export default NewSubmissions
