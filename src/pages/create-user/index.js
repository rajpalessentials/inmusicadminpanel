// ** MUI Imports
import { useState } from 'react'
import axios from 'axios'
import TextField from '@mui/material/TextField'
import Grid from '@mui/material/Grid'
import CardHeader from '@mui/material/CardHeader'
import Button from '@mui/material/Button'
import CardContent from '@mui/material/CardContent'
import Card from '@mui/material/Card'

const CreateUser = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const handleSaveEmail = event => {
    setEmail(event.target.value)
    console.log(event.target.value)
  }

  const handleSavePassword = event => {
    setPassword(event.target.value)
    console.log(event.target.value)
  }

  const handleSubmit = async () => {
    const token =
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjg3MDk2NDEyLCJleHAiOjE2ODcwOTY3MTJ9.ylTuco0Y5qEVl3uaA0dopd0shqTrBXSJN340ot88Uks'

    const response = await axios.post(
      `https://api.inmusicdigital.com/v4/createnewuser?email=${email}&password=${password}`,
      {},
      {
        headers: {
          'X-Auth-Token': token
        }
      }
    )
    console.log('dddddddddd', response.data)
  }

  return (
    <Grid container spacing={6}>
      <Grid item xs={5}>
        <Card>
          <CardHeader title='Create a new user'></CardHeader>
          <CardContent>
            <Grid>
              <TextField
                sx={{ marginLeft: '50px', marginTop: '20px' }}
                required
                autoFocus
                label='Email'
                name='email'
                value={email}
                onChange={e => handleSaveEmail(e)}
                placeholder='user@inmusic.com'
              />
              <TextField
                sx={{ marginLeft: '50px', marginTop: '20px' }}
                required
                autoFocus
                label='Password'
                name='password'
                value={password}
                onChange={e => handleSavePassword(e)}
                placeholder='abcd@1234'
              />
            </Grid>
            <Button
              sx={{ border: '#9155FD solid 1.5px', marginLeft: '200px', marginTop: '40px', marginBottom: '20px' }}
              onClick={handleSubmit}
            >
              Add User
            </Button>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  )
}

export default CreateUser
