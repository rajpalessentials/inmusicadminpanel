// ** React Imports
import { useState, Fragment, useEffect } from 'react'

// ** MUI Imports
import Box from '@mui/material/Box'
import Paper from '@mui/material/Paper'
import Button from '@mui/material/Button'
import Table from '@mui/material/Table'
import Collapse from '@mui/material/Collapse'
import TableRow from '@mui/material/TableRow'
import TableHead from '@mui/material/TableHead'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import Typography from '@mui/material/Typography'
import IconButton from '@mui/material/IconButton'
import TableContainer from '@mui/material/TableContainer'
import axios from 'axios'


// ** Icon Imports
import Icon from 'src/@core/components/icon'


const Row = props => {
  // ** Props
  const [releaseDraft, setReleaseDraft] = useState([]);

  const [trackDraft, setTrackDraft] = useState([]);

  // useEffect(() => {
  //   const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFyeWFuMDQwNTAxQGdtYWlsLmNvbSIsImlkIjoxfQ.MtTr_aSYgx21iH2Rd43h6mDDIx7r9gd9deNCkGKWIl0'
  //   async function getUserSubmissions() {
  //     const res = await axios.get('https://api.inmusicdigital.com/v4/release/submitted', {
  //       headers: {
  //         'X-Auth-Token': token
  //       }
  //     })

  //     const res1 = await axios.get(`https://api.inmusicdigital.com/v4/${res.data.id}/tracks`, {
  //       headers: {
  //         'X-Auth-Token': token
  //       }
  //     })

  //     setReleaseDraft(res.data)
  //     setTrackDraft(res1.data);
  //   }
  //   getUserSubmissions()
  // })
  const getDrafts = async (id) => {
    setOpen(!open);

    const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFyeWFuMDQwNTAxQGdtYWlsLmNvbSIsImlkIjoxfQ.MtTr_aSYgx21iH2Rd43h6mDDIx7r9gd9deNCkGKWIl0'

    const res = await axios.get('https://api.inmusicdigital.com/v4/release/submitted', {
      headers: {
        'X-Auth-Token': token
      }
    })

    const res1 = await axios.get(`https://api.inmusicdigital.com/v4/${id}/tracks`, {
      headers: {
        'X-Auth-Token': token
      }
    })
    for (let i = 0; i < res.data.length; i++) {
      if (res.data[i].id === id) {
        setReleaseDraft(res.data[i])
      }
    }
    setTrackDraft(res1.data);
  }

  const publishRelease = async releaseID => {
    const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFyeWFuMDQwNTAxQGdtYWlsLmNvbSIsImlkIjoxfQ.MtTr_aSYgx21iH2Rd43h6mDDIx7r9gd9deNCkGKWIl0'

    const res = await axios.post((`https://api.inmusicdigital.com/v4/${releaseID}/verify-release`), {
      headers: {
        'X-Auth-Token': token
      }
    })
    location.reload()
  }


  const { row } = props

  // ** State
  const [open, setOpen] = useState(false)

  return (
    <Fragment>
      <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
        <TableCell>
          <IconButton aria-label='expand row' size='small' onClick={() => getDrafts(row.id)}>
            <Icon icon={open ? 'mdi:chevron-up' : 'mdi:chevron-down'} />
          </IconButton>
        </TableCell>
        <TableCell component='th' scope='row'>
          {row.title}
        </TableCell>
        <TableCell align='right'>{row.userID}</TableCell>
        <TableCell align='right'>{row.upc}</TableCell>
        <TableCell align='right'>{row.submissionDate.substring(0, row.submissionDate.indexOf(' '))}</TableCell>
        <TableCell align='right'>{row.status === "" ? <Button onClick={() => publishRelease(row.id)}>Verify</Button> : "Verified"}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell colSpan={6} sx={{ py: '0 !important' }}>
          <Collapse in={open} timeout='auto' unmountOnExit>
            <Box sx={{ m: 2 }}>
              <Typography sx={{ ml: 5, mt: 6, fontWeight: 600 }}>Release Information</Typography>
              <TableContainer>
                <Table>
                  {releaseDraft && (
                    <TableBody>
                      <TableRow>
                        <TableCell>Title</TableCell>
                        <TableCell>{releaseDraft.title}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>Version</TableCell>
                        <TableCell>{releaseDraft.version}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>Primary Artists</TableCell>
                        <TableCell>
                          {releaseDraft.primaryArtists &&
                            releaseDraft.primaryArtists.map((da, i) => <div key={i}>{da.artistID}</div>)}
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>Featuring Artists</TableCell>
                        <TableCell>
                          {releaseDraft.featuringArtists &&
                            releaseDraft.featuringArtists.map((da, i) => <div key={i}>{da.artistID}</div>)}
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>Genre</TableCell>
                        <TableCell>{releaseDraft.genre}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>Subgenre</TableCell>
                        <TableCell>{releaseDraft.subGenre}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>Label</TableCell>
                        <TableCell>{releaseDraft.label}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>Format</TableCell>
                        <TableCell>{releaseDraft.format}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>CopyrightHolder</TableCell>
                        <TableCell>{releaseDraft.copyrightHolder}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>CopyrightYear</TableCell>
                        <TableCell>{releaseDraft.copyrightYear}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>Upc</TableCell>
                        <TableCell>{releaseDraft.upc}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>Producer Catalogue Number</TableCell>
                        <TableCell>{releaseDraft.producerCatelogueNumber}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>Price Tier</TableCell>
                        <TableCell>{releaseDraft.priceTier}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>Original Release Date</TableCell>
                        <TableCell>{releaseDraft.originalReleaseDate}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>Main Release Date</TableCell>
                        <TableCell>{releaseDraft.mainReleaseDate}</TableCell>
                      </TableRow>
                    </TableBody>
                  )}
                </Table>
              </TableContainer>

              <Typography sx={{ ml: 5, mt: 15, fontWeight: 600 }}>Track Information</Typography>
              <TableContainer>
                <Table>
                  {trackDraft.length &&
                    trackDraft.map((d, index) => (
                      <TableBody key={index}>
                        <div>Track {index + 1}</div>
                        <TableRow>
                          <TableCell>Title</TableCell>
                          <TableCell>{trackDraft[index].title}</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Subtitle</TableCell>
                          <TableCell>{trackDraft[index].subTitle}</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Primary Artists</TableCell>
                          <TableCell>
                            {trackDraft[index].trackPrimaryArtistList &&
                              trackDraft[index].trackPrimaryArtistList.map((da, i) => <div key={i}>{da.artistID}</div>)}
                          </TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Featuring Artists</TableCell>
                          <TableCell>
                            {trackDraft[index].trackFeaturings &&
                              trackDraft[index].trackFeaturings.map((da, i) => <div key={i}>{da.artistID}</div>)}
                          </TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Authors</TableCell>
                          <TableCell>
                            {trackDraft[index].trackAuthors &&
                              trackDraft[index].trackAuthors.map((da, i) => <div key={i}>{da.name}</div>)}
                          </TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Composers</TableCell>
                          <TableCell>
                            {trackDraft[index].trackComposers &&
                              trackDraft[index].trackComposers.map((da, i) => <div key={i}>{da.name}</div>)}
                          </TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Arrangers</TableCell>
                          <TableCell>
                            {trackDraft[index].trackArrangers &&
                              trackDraft[index].trackArrangers.map((da, i) => <div key={i}>{da.name}</div>)}
                          </TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Producers</TableCell>
                          <TableCell>
                            {trackDraft[index].trackProducers &&
                              trackDraft[index].trackProducers.map((da, i) => <div key={i}>{da.name}</div>)}
                          </TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Remixers</TableCell>
                          <TableCell>
                            {trackDraft[index].trackRemixers &&
                              trackDraft[index].trackRemixers.map((da, i) => <div key={i}>{da.remixerID}</div>)}
                          </TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Producer Catalogue Number</TableCell>
                          <TableCell>{trackDraft[index].producerCatalogueNumber}</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Publishin Year</TableCell>
                          <TableCell>{trackDraft[index].publishingYear}</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Publishing Holder</TableCell>
                          <TableCell>{trackDraft[index].publishingHolder}</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>ISRC</TableCell>
                          <TableCell>{trackDraft[index].ISRC}</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Genre</TableCell>
                          <TableCell>{trackDraft[index].genre}</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Subgenre</TableCell>
                          <TableCell>{trackDraft[index].subGenre}</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Price</TableCell>
                          <TableCell>{trackDraft[index].price}</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Lyrics Language</TableCell>
                          <TableCell>{trackDraft[index].lyricsLanguage}</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Parental Advisory</TableCell>
                          <TableCell>{trackDraft[index].parentalAdvisory}</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Lyrics</TableCell>
                          <TableCell>{trackDraft[index].lyrics}</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Territories</TableCell>
                          <TableCell>
                            {trackDraft[index].trackTerritorys &&
                              trackDraft[index].trackTerritorys.map((da, i) => <div key={i}>{da.territoryID}</div>)}
                          </TableCell>
                        </TableRow>
                      </TableBody>
                    ))}
                </Table>
              </TableContainer>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </Fragment>
  )
}

const NewSubmissions = () => {

  const [submittedRelease, setSubmittedRelease] = useState([])

  useEffect(() => {
    const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFyeWFuMDQwNTAxQGdtYWlsLmNvbSIsImlkIjoxfQ.MtTr_aSYgx21iH2Rd43h6mDDIx7r9gd9deNCkGKWIl0'
    async function getUserSubmissions() {
      const res = await axios.get('https://api.inmusicdigital.com/v4/release/submitted', {
        headers: {
          'X-Auth-Token': token
        }
      })
      setSubmittedRelease(res.data)
    }
    getUserSubmissions()
  }, [])

  return (
    <TableContainer component={Paper}>
      <Table aria-label='collapsible table'>
        <TableHead>
          <TableRow>
            <TableCell />
            <TableCell>Album Title</TableCell>
            <TableCell align='right'>User Id</TableCell>
            <TableCell align='right'>Upc</TableCell>
            <TableCell align='right'>Submission Date</TableCell>
            <TableCell align='right'>Verify</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {submittedRelease.map(row => (
            <Row key={row.id} row={row} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}

export default NewSubmissions
