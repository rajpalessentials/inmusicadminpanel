// ** MUI Components
import useMediaQuery from '@mui/material/useMediaQuery'
import { styled, useTheme } from '@mui/material/styles'

// Styled Components

const Tree1Img = styled('img')(() => ({
  left: 0,
  bottom: 0,
  position: 'absolute',
  height: '100%',
  width: '100%'
}))

const FooterIllustrationsV1 = props => {
  // ** Props
  const { image1 } = props

  // ** Hook
  const theme = useTheme()

  // ** Vars
  const hidden = useMediaQuery(theme.breakpoints.down('md'))
  if (!hidden) {
    return <>{image1 || <Tree1Img alt='tree' src='/images/pages/login-2.avif' />}</>
  } else {
    return null
  }
}

export default FooterIllustrationsV1
